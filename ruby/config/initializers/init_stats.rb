require 'influx_stats'

class Stats
  def self.timing(metric, duration = nil, &block)
    instance.timing(metric, duration, &block)
  end

  def self.instance
    @instance ||= InfluxStats.new
  end
end
