require 'vips_renderer'

module Renderer
  @renderer = VipsRenderer.new

  def self.backend
    @renderer
  end
end
