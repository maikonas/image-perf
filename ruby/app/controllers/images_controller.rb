class ImagesController < ApplicationController
  def show
    start_time = request.headers['X-Request-Start']
    Stats.timing('queue', ((Time.now.to_r - start_time.to_r) * 10**9).to_i)

    Stats.timing('request') do
      bytes = Stats.timing('read') do
        Store.backend.load("#{params[:id]}.#{params[:format]}")
      end

      width, height = params[:size].split('x').map(&:to_i)
      resized = Stats.timing('render') do
        Renderer.backend.thumbnail(bytes, width, height)
      end

      send_data(resized, type: 'image/jpeg', disposition: :inline)
    end
  end
end
