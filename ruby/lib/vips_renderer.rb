class VipsRenderer
  def thumbnail(bytes, width, height)
    img = Vips::Image.new_from_buffer(bytes, '')


    resize_ratio = [width, height].zip(img.size).map { |f, s| f / s.to_f }.max
    img = img.resize(resize_ratio, kernel: :cubic) if resize_ratio < 1

    img.jpegsave_buffer(Q: 90)
  end
end
