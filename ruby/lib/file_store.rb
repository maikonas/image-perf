class FileStore
  def initialize(base_path)
    @base_path = base_path
  end

  def load(id)
    File.read(File.join(@base_path, id))
  end
end
