class InfluxStats
  MAX_POINTS = 1_000

  def initialize
    @influxdb = InfluxDB::Client.new('metrics', time_precision: 'ns')
    @queue = Queue.new
    process_writes!
  end

  def write_point(point)
    @queue << point
  end

  def timing(metric, duration)
    if duration.nil?
      from = Time.now
      result = yield
      to = Time.now
      duration = time_to_nano(from, to)
    end

    write_point(
      series: 'duration',
      values:    { value: duration },
      tags:      { metric: metric, lang: 'ruby' },
      timestamp: (Time.now.to_r * 10**9).to_i,
    )
    result
  end

  private

  def write_buffer(buffer)
    @influxdb.write_points(buffer)
  rescue => e
    puts e
  end

  def process_writes!
    Thread.new do
      buffer = []
      last_written = Time.now
      loop do
        buffer << @queue.pop

        if buffer.size > MAX_POINTS || (Time.now - last_written) > 5.seconds
          write_buffer(buffer)
          last_written = Time.now
          buffer = []
        end
      end
    end
  end

  def time_to_nano(from, to)
    ((to.to_r - from.to_r) * 10**9).to_i
  end
end
