package image

import (
	"github.com/andrewgleave/govips"
)

var defaultOptions *govips.Options

// GoVipsRenderer .
type GoVipsRenderer struct{}

// Initialize .
func (GoVipsRenderer) Initialize() error {
	govips.Startup(nil)
	return nil
}

// Shutdown .
func (GoVipsRenderer) Shutdown() {
	govips.Shutdown()
}

// Thumbnail .
func (GoVipsRenderer) Thumbnail(image []byte, width int, height int) ([]byte, error) {
	defer govips.ShutdownThread()
	img, err := govips.NewImageFromBuffer(image, nil)

	if err != nil {
		return nil, err
	}

	factor := resizeFactor(img.Width(), img.Height(), width, height)
	if factor < 1 {
		img = img.ResizeEx(factor, resizeOptions())
	}

	left := (img.Width() - width) / 2
	top := (img.Height() - height) / 2

	img = img.ExtractArea(
		max(0, left),
		max(0, top),
		min(img.Width(), width),
		min(img.Height(), height),
	)

	return img.JpegsaveBuffer().ToBytes(), nil
}

func resizeOptions() *govips.Options {
	if defaultOptions != nil {
		return defaultOptions
	}

	interpolator, err := govips.NewInterpolator("bicubic")
	if err != nil {
		panic(err)
	}
	defaultOptions = govips.NewOptions().SetInterpolator("interpolate", interpolator)
	return defaultOptions
}
