package image

import (
	"io/ioutil"
	"testing"
)

func TestBimgThumbnail(t *testing.T) {
	renderer := BimgRenderer{}
	renderer.Initialize()
	defer renderer.Shutdown()

	bytes, _ := ioutil.ReadFile("test_data/image.jpeg")

	out, err := renderer.Thumbnail(bytes, 100, 100)
	if err != nil {
		t.Error(err)
	}
	ioutil.WriteFile("test_data/out-bimg.jpeg", out, 0644)
}
