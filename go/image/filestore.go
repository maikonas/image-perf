package image

import (
	"io/ioutil"
	"path/filepath"
)

// FileStore .
type FileStore struct {
	BasePath string
}

// GetBytes .
func (p FileStore) GetBytes(id string) ([]byte, error) {
	return ioutil.ReadFile(filepath.Join(p.BasePath, id))
}
