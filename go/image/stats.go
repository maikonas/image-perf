package image

import (
	"fmt"
	"log"
	"time"

	influx "github.com/influxdata/influxdb/client/v2"
)

// Stats .
type Stats interface {
	MeasureTime(metric string) TimeMeasurement
	ReportTime(metric string, duration time.Duration) error
}

// TimeMeasurement .
type TimeMeasurement interface {
	End() error
}

type influxStats struct {
	Address string
	DB      string

	influxClient influx.Client
	pointBuffer  chan *influx.Point
}

type influxTimeMeasurement struct {
	startTime time.Time
	metric    string
	stats     *influxStats
}

// NewInfluxStats .
func NewInfluxStats() Stats {
	return NewInfluxStatsFrom("http://localhost:8086", "metrics")
}

// NewInfluxStatsFrom .
func NewInfluxStatsFrom(address string, db string) Stats {
	stats := &influxStats{Address: address, DB: db}
	stats.start()
	return stats
}

func (stats *influxStats) start() {
	var err error

	stats.influxClient, err = influx.NewHTTPClient(influx.HTTPConfig{
		Addr:    stats.Address,
		Timeout: time.Millisecond * 5000,
	})
	if err != nil {
		log.Printf("Error while creating influx cient %v", err)
	}

	stats.pointBuffer = make(chan *influx.Point, 500)

	go stats.processBatch()
}

const (
	bufferCapacity = 1000
)

type value map[string]interface{}

type tag map[string]string

// MeasureTime .
func (stats *influxStats) MeasureTime(metric string) TimeMeasurement {
	return &influxTimeMeasurement{startTime: time.Now(), stats: stats, metric: metric}
}

func (stats *influxStats) ReportTime(metric string, duration time.Duration) error {
	point, err := influx.NewPoint(
		"duration",
		tag{"metric": metric, "lang": "go"},
		value{"value": duration.Nanoseconds()},
		time.Now(),
	)

	if err != nil {
		return err
	}
	stats.pointBuffer <- point
	return nil

}

func (stats *influxStats) flushBuffer(buffer []*influx.Point) []*influx.Point {
	err := stats.writePoints(buffer)

	if err != nil {
		log.Printf("Error %v while adding points, %d points lost", err, len(buffer))
	} else {
		log.Printf("Written %d points to influx", len(buffer))
	}
	return make([]*influx.Point, 0, bufferCapacity)
}

func (stats *influxStats) writePoints(buffer []*influx.Point) error {
	if stats.influxClient == nil {
		return fmt.Errorf("Client is not responding")
	}

	batch, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: stats.DB,
	})
	if err != nil {
		return err
	}
	batch.AddPoints(buffer)
	return stats.influxClient.Write(batch)
}

func (stats *influxStats) processBatch() {
	buffer := make([]*influx.Point, 0, bufferCapacity)
	for {
		select {
		case <-time.After(5 * time.Second):
			if len(buffer) == 0 {
				continue
			}
			buffer = stats.flushBuffer(buffer)
		case point := <-stats.pointBuffer:
			buffer = append(buffer, point)

			if len(buffer) >= bufferCapacity {
				buffer = stats.flushBuffer(buffer)
			}
		}
	}
}

func (m *influxTimeMeasurement) End() error {
	point, err := influx.NewPoint(
		"duration",
		tag{"metric": m.metric, "lang": "go"},
		value{"value": int64(time.Since(m.startTime))},
		time.Now(),
	)

	if err != nil {
		return err
	}
	m.stats.pointBuffer <- point
	return nil
}
