package image

import (
	"math"
	"strconv"
	"time"
)

// BaseStore .
type BaseStore interface {
	// GetBytes .
	GetBytes(id string) ([]byte, error)
}

// Renderer .
type Renderer interface {
	Initialize() error
	// Thumbnail .
	Thumbnail(image []byte, width int, height int) ([]byte, error)
	Shutdown()
}

func resizeFactor(oldWidth int, oldHeight int, width int, height int) float64 {
	return maxFloat64(
		div(width, oldWidth),
		div(height, oldHeight),
	)
}

func div(first int, second int) float64 {
	return float64(first) / float64(second)
}

func max(first int, second int) int {
	if first > second {
		return first
	}
	return second
}

func min(first int, second int) int {
	if first < second {
		return first
	}
	return second
}

func maxFloat64(first float64, second float64) float64 {
	if first > second {
		return first
	}
	return second
}

// FloatStrToTime .
func FloatStrToTime(timestamp string) time.Time {
	floatTime, _ := strconv.ParseFloat(timestamp, 64)
	sec, dec := math.Modf(floatTime)
	return time.Unix(int64(sec), int64(dec*1e9))
}
