package image

import "github.com/h2non/bimg"

// BimgRenderer .
type BimgRenderer struct{}

// Initialize .
func (BimgRenderer) Initialize() error {
	bimg.Initialize()
	return nil
}

// Thumbnail .
func (BimgRenderer) Thumbnail(image []byte, width int, height int) ([]byte, error) {
	img := bimg.NewImage(image)

	return img.ResizeAndCrop(width, height)
}

// Shutdown .
func (BimgRenderer) Shutdown() {
	bimg.Shutdown()
}
