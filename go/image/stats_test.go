package image

import (
	"testing"
)

func TestStats(t *testing.T) {
	stats := NewInfluxStatsFrom("http://localhost:8086", "test")
	err := stats.MeasureTime("hello_world").End()
	if err != nil {
		t.Error(err)
	}
	// time.Sleep(6 * time.Second)
}
