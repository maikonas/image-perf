package image

import (
	"io/ioutil"
	"sync"
	"testing"
)

func TestGoVipsThumbnail(t *testing.T) {
	renderer := GoVipsRenderer{}
	renderer.Initialize()
	defer renderer.Shutdown()

	bytes, _ := ioutil.ReadFile("test_data/image.jpeg")

	out, err := renderer.Thumbnail(bytes, 100, 100)
	if err != nil {
		t.Error(err)
	}
	ioutil.WriteFile("test_data/out.jpeg", out, 0644)
}

func TestMultiThreadedSetup(t *testing.T) {
	renderer := GoVipsRenderer{}
	renderer.Initialize()
	defer renderer.Shutdown()

	bytes, _ := ioutil.ReadFile("test_data/image.jpeg")
	wg := sync.WaitGroup{}

	const N = 2
	wg.Add(N)
	for i := 0; i < N; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				renderer.Thumbnail(bytes, 100, 100)
			}
		}()
	}
	wg.Wait()
}
