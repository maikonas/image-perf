package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/maikonas/image-perf/go/image"
)

var store image.BaseStore
var renderer image.Renderer
var stats image.Stats

type renderParams struct {
	ID     string
	Width  int
	Height int
}

func getParams(r *http.Request) (*renderParams, error) {
	params := renderParams{}
	_, err := fmt.Sscanf(r.URL.Path, "/image/%dx%d/%s", &params.Width, &params.Height, &params.ID)
	if err != nil {
		return nil, err
	}
	return &params, nil
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	defer stats.MeasureTime("request").End()

	stats.ReportTime("queue", time.Since(image.FloatStrToTime(r.Header.Get("X-Request-Start"))))

	params, err := getParams(r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	readStats := stats.MeasureTime("read")
	bytes, err := store.GetBytes(params.ID)
	readStats.End()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	renderStats := stats.MeasureTime("render")
	resized, err := renderer.Thumbnail(bytes, params.Height, params.Width)
	renderStats.End()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Lenght", fmt.Sprint(len(resized)))
	w.Header().Set("Content-Type", "image/jpeg")

	w.Write(resized)
}

func main() {
	store = image.FileStore{BasePath: "/Users/raimondasv/Pictures/small"}
	renderer = image.BimgRenderer{}
	defer renderer.Shutdown()

	stats = image.NewInfluxStats()

	http.HandleFunc("/image/", handleRequest)
	http.ListenAndServe(":12345", nil)
}
